from distutils.core import setup

setup(
    name="EXbee",
    version='0.0.1',
    py_modules=['EXbee'],
    author='Wester de Weerdt',
    author_email='wester.de.weerdt@dutchmail.com',
    url='https://github.com/',
    description='Python wrapper for XBee Pro 3B Programmable',
    license='MIT'
)
